import os
from setuptools import setup

local = 'CI_COMMIT_REF_NAME' not in os.environ
dev = 'CI_COMMIT_TAG' not in os.environ
if not dev:
    version = os.environ['CI_COMMIT_TAG']
else:
    version = '0.dev1'  # TODO: have something more elaborate here

setup(name='twitchbot-core',
      packages=['twitchbot'],
      description='twitchbot',
      version=version,
      url='',
      author='Henny022',
      install_requires=['irc'])
