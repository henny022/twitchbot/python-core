import twitchbot


def main():
    twitchbot.start_bot(twitchbot.const.channel, twitchbot.const.username, twitchbot.const.token)
    pass


if __name__ == '__main__':
    main()
