import os
import json

const_filename = 'const.json'
const = None


def get_const(name):
    global const
    if not const:
        if os.path.exists(const_filename):
            with open(const_filename) as constfile:
                const = json.load(constfile)
        else:
            raise ValueError(f'neither env variables nor {const_filename} found')
    return const[name]


channel = os.environ.get('TWITCHBOT_CHANEL') or get_const('channel')
username = os.environ.get('TWITCHBOT_USERNAME') or get_const('username')
token = os.environ.get('TWITCHBOT_TOKEN') or get_const('token')
client_id = os.environ.get('TWITCHBOT_CLIENT_ID') or get_const('client_id')
