import irc.client
import sys


class TwitchBot(irc.client.SimpleIRCClient):
    def __init__(self, channel, username, token):
        self.irc_channel = f'#{channel}'
        self.irc_nick = username
        self.irc_pass = f'oauth:{token}'
        self.irc_server = 'irc.chat.twitch.tv'
        self.irc_port = 6667
        self.stopped = False
        super().__init__()

    def connect(self, *args, **kwargs):
        return super(TwitchBot, self).connect(self.irc_server, self.irc_port, self.irc_nick, password=self.irc_pass)

    def on_welcome(self, connection, event):
        connection.join(self.irc_channel)

    def on_join(self, connection, event):
        pass

    def on_pubmsg(self, connection, event):
        print(event.arguments[0])

    def run(self):
        while not self.stopped:
            self.reactor.process_once(0.2)

    def stop(self):
        self.connection.disconnect(f'{self.irc_nick} out')
        self.stopped = True


def start_bot(channel, name, token):
    client = TwitchBot(channel, name, token)
    try:
        client.connect()
    except irc.client.ServerConnectionError as e:
        print(e)
        sys.exit(1)
    client.run()
